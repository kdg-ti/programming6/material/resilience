package be.kdg.prog6.patterns.shakyrestservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShakyRestServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShakyRestServiceApplication.class, args);
    }

}
