package be.kdg.prog6.patterns.shakyrestservice;

import jakarta.websocket.server.PathParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
public class HelloWorldController {

    private static AtomicInteger counter = new AtomicInteger(0);
    private static AtomicInteger breaker = new AtomicInteger(0);


    @GetMapping("/retry/hello/{name}")
    public String retryHello(@PathVariable String name){
        if (counter.incrementAndGet() % 5 != 0){
            throw new RuntimeException("counter == " + counter.get());
        } else {
            return "Hello " + name;
        }
    }


    @GetMapping("/limit/hello/{name}")
    public String limitHello(@PathVariable String name) throws InterruptedException {
        System.out.println("This will take a long time!!!!!!");
        Thread.sleep(60 *1000l);
        return "Hello " + name;
    }


    @GetMapping("/breaker/hello/{name}")
    public String breakHello(@PathVariable String name) throws InterruptedException {
        System.out.println("Breaker got called!!!! " + breaker.getAndIncrement());
        throw new RuntimeException("Not working!!!!");
    }

    @GetMapping("/cacheme")
    public String getUUID() {return UUID.randomUUID().toString();
    }



}
