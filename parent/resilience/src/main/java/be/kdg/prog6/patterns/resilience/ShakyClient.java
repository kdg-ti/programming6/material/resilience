package be.kdg.prog6.patterns.resilience;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.function.Supplier;

@FeignClient("shakyservice")
public interface ShakyClient {

    @GetMapping("/retry/hello/{name}")
    String retryHello(@PathVariable String name);

    @GetMapping("/limit/hello/{name}")
    String limitHello(@PathVariable String name);

    @GetMapping("/breaker/hello/{name}")
    String breakerHello(@PathVariable String name);

    @GetMapping("/cacheme")
    String cacheme();

}
