package be.kdg.prog6.patterns.resilience;

import io.github.resilience4j.bulkhead.annotation.Bulkhead;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import io.github.resilience4j.timelimiter.annotation.TimeLimiter;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Service
public class ShakyCaller {

    private final ShakyClient shakyClient;

    public ShakyCaller(ShakyClient shakyClient) {
        this.shakyClient = shakyClient;
    }

    @Retry(name = "retryRemote", fallbackMethod = "fallback")
    public String retryRemote(){
        return shakyClient.retryHello("Kevin");
    }

    @CircuitBreaker(name = "breakerRemote", fallbackMethod = "fallback")
    public String breakerRemote(){
        return shakyClient.breakerHello("Kevin");
    }


    @Bulkhead(name = "helloBulkhead", fallbackMethod = "fallbackLimit", type = Bulkhead.Type.THREADPOOL)
    @TimeLimiter(name = "limitRemote", fallbackMethod = "fallbackLimit")
    public CompletableFuture<Void> limitRemote(){
        return CompletableFuture.runAsync(() -> shakyClient.limitHello("Kevin"));
    }


    @Cacheable("uuids")
    public String cacheMe(){
        return shakyClient.cacheme();
    }

    @CacheEvict("uuids")
    public String cacheEvict(){
        return shakyClient.cacheme();
    }


    public String fallback(Throwable throwable){
        System.out.println("FALLBACK");
        return "No hello for you, falling back!!!!";
    }

    public CompletableFuture<String> fallbackLimit(Throwable throwable){
        System.out.println(">>>>>>> LIMIT FALLBACK");
        return CompletableFuture.completedFuture("No hello for you, falling back!!!!");
    }

}
