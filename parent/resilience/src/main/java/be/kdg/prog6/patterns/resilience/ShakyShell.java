package be.kdg.prog6.patterns.resilience;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@ShellComponent
public class ShakyShell {

    private final ShakyCaller shakyCaller;

    public ShakyShell(ShakyCaller shakyCaller) {
        this.shakyCaller = shakyCaller;
    }



    @ShellMethod(value= "Retry shaky service")
    public void retryShakyService(){
        System.out.println(shakyCaller.retryRemote());
    }

    @ShellMethod(value= "Limit shaky service")
    public void limitShakyService(){
        System.out.println(shakyCaller.limitRemote().join());
    }

    @ShellMethod(value= "Breaker shaky service")
    public void breakerShakyService(int calls){
        while (calls-- > 0) {
            System.out.println(shakyCaller.breakerRemote());
        }
    }

    @ShellMethod(value= "CacheMe")
    public void cacheMe(){
        System.out.println(shakyCaller.cacheMe());
    }

    @ShellMethod(value= "CacheEvict")
    public void cacheEvict(){
        System.out.println(shakyCaller.cacheEvict());
    }


}
