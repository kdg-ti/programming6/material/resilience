package be.kdg.prog6.patterns.resilience;

import org.jline.utils.AttributedString;
import org.springframework.shell.jline.PromptProvider;
import org.springframework.stereotype.Component;

@Component
public class ShellConfig implements PromptProvider {

    @Override
    public AttributedString getPrompt() {
        return new AttributedString("shaky-shell:>");
    }
}
